source .env
echo "Backup of MQTT broker..."
mkdir backups/mosquitto/
docker cp broker:/mosquitto/data/mosquitto.db backups/mosquitto/
echo "... done."
echo "---------------------------------------------------"
echo "Backup ${DATABASE_MONITORING_DB} database..."
docker exec -t database influx backup /backups/
docker exec -t database chown $UID:$GID -R /backups/
echo "... done."
echo "---------------------------------------------------"
echo "Backup database of visualisation..."
docker exec -t visualization_db pg_dump -U visualization visualization -f /docker-entrypoint-initdb.d/db.sql
echo "... done."
