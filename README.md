# EoT - EnvoT- Environment of Things

Environment of Things (EoT) is a collection of software forming a framework to configure, control and monitor devices in your secure environment like your home, lab ...
The idea and implementation are derived from the Internet of Things (IoT) which is commonly used in "Smart" environments.

```mermaid
graph TB
    subgraph User interfaces
    B{{Web GUI}} & C{{Visualization}}
    end
    B --> A
    A --> B

    D --> C

    A((MQTT Broker))
    E[Exporter]
    D[Database]

    A --> E --> D

    F[/Device0\] --- A
    G[/Device1\] --- A
    H[/Device2\] --- A

    subgraph Devices
    F & G & H
    end

    style A fill:#0f0,stroke:#333,stroke-width:3px
    style F fill:#f71
    style G fill:#f71
    style H fill:#f71
```

## Building blocks

* MQTT Broker: [Mosquitto](https://mosquitto.org)
* Exporter: [MQTT 2 Influx](https://gitlab.com/envot/mqtt2influx)
* Time series database: [InfluxDB](https://www.influxdata.com/products/influxdb-overview)
* Interactive visualization: [Grafana](https://grafana.com)
* Web-based GUI: [Adjusted HoDD](https://gitlab.com/schueppi/hodd/)
* Devices with MQTT compatibility or self-programmed software with MQTT connectivity

In general, every MQTT communication could be controlled and monitored via this framework.
However, this projects follows the [Homie convention](https://homieiot.github.io) so that properties of devices structured in nodes are fully defined and configured by the information published via the MQTT Broker.
A modular python implementation following the Homie convention is [DeVisor](https://gitlab.com/envot/devisor) which is used here.

## Get started

As our EoT implementation, is build in a microservices architecture with a lot of separated instances connected via different protocols, the simplest way to orchestrate EoT is the Docker Engine with Docker-Compose in a platform as a service (PaaS).

### Install Docker: 

* [Debian](https://docs.docker.com/install/linux/docker-ce/debian)
* [Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu)

### Install Docker-Compose: 

[https://docs.docker.com/compose/install](https://docs.docker.com/compose/install)


### Start

* Quickstart without git

  > sh <(curl -L -s envot.io/quickstart)


* Run with git

  > docker-compose up -d

  to start the environment with a test [DeVisor](https://gitlab.com/envot/devisor).


### Further commands

* Save containers' data in "backups" 

  > ./backup.sh

* Overwrite the docker volume with the data in "backups",

  > ./restore.sh

* Check running containers:

  > docker ps -a

* Shutdown (stop and remove) all EoT containers:

  > docker-compose down

* Remove the docker volumes

  > ./clean.sh

* Start MesQuesh (interactive MQTT bash):

  > docker run -it --rm envot/mesquesh

* Start another DeVisor instance:

  > docker run -it --rm --network='host' -e "HOST=localhost" envot/devisor


## Alternatives

### MQTT Broker
* [Kafka](https://kafka.apache.org) proxy/bridge for example by [Confluent](https://www.confluent.io): interesting if you want to scale your system to millions of queries per second.
* [HiveMQ](https://www.hivemq.com)

### Web GUI
* [OpenHAB](https://www.openhab.org) customizable by HabPanel.
* [Home Assistent](https://www.home-assistant.io)

### Exporter
* With [Kafka](https://kafka.apache.org) proxy/bridge, an exporter would be redundant
    as Kafka would act as the MQTT Broker and a platform to stream the data.
* [Telegraf](https://www.influxdata.com/time-series-platform/telegraf): limited convert possibilities.

### Visualization
* [Netdata](https://www.netdata.cloud)
* built-in solutions of databases

### Database
* [OpenTSDB](http://opentsdb.net): complex to setup as it runs on Hadoop and HBase.
* [Graphite](http://graphiteapp.org)
* [Prometheus](https://prometheus.io) and [Thanos](https://thanos.io): scrapes data and only allows pushing as an exception.
