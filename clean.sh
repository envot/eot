# /bin/bash
source .env

echo 'Remove docker volumes...'
vollist=$(docker volume ls -q )
for var in $vollist
do
    if [[ $var == *"${PROJECT_NAME}"* ]]; then
        docker volume rm $var
    fi
done
echo "... done."
echo "---------------------------------------------------"

echo 'Considuer to remove backups:'
echo 'Broker, databases and visualisation automatically load their backup at the next startup.'
